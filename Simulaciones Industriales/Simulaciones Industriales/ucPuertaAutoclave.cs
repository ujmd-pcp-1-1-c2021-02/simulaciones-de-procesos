﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulaciones_Industriales
{
    public partial class ucPuertaAutoclave : UserControl
    {
        public ucPuertaAutoclave()
        {
            InitializeComponent();
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            tmrAbrir.Enabled = true;
            tmrCerrar.Enabled = false;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            tmrAbrir.Enabled = false;
            tmrCerrar.Enabled = true;
        }

        private void tmrAbrir_Tick(object sender, EventArgs e)
        {
            if (panelPuerta.Height > 0)
            {
                //panelPuerta.Height = panelPuerta.Height - 4;
                panelPuerta.Height -= 4;
                panelPuerta.Top += 4;
            }
            else
            {
                tmrAbrir.Enabled = false;
            }
        }

        private void tmrCerrar_Tick(object sender, EventArgs e)
        {
            if (panelPuerta.Height < 164)
            {
                panelPuerta.Height += 4;
                panelPuerta.Top -= 4;
            }
            else
            {
                tmrCerrar.Enabled = false;
            }
        }
    }
}
