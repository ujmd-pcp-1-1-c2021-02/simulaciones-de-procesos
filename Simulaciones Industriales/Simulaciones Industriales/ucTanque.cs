﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simulaciones_Industriales
{
    public partial class ucTanque : UserControl
    {
        public ucTanque()
        {
            InitializeComponent();
        }

        private void btnLlenar_Click(object sender, EventArgs e)
        {
            tmrLlenado.Enabled = true;
            tmrVaciado.Enabled = false;
        }

        private void btnVaciar_Click(object sender, EventArgs e)
        {
            tmrVaciado.Enabled = true;
            tmrLlenado.Enabled = false;
        }

        private void tmrLlenado_Tick(object sender, EventArgs e)
        {
            if (panelLíquido.Height < panelTanque.Height)
            {
                panelLíquido.Height += 2;
            }
            else
            {
                tmrLlenado.Enabled = false;
            }
        }

        private void tmrVaciado_Tick(object sender, EventArgs e)
        {
            if (panelLíquido.Height > 0)
            {
                panelLíquido.Height -= 2;
            }
            else
            {
                tmrVaciado.Enabled = false;
            }
        }

        private void tmrVariables_Tick(object sender, EventArgs e)
        {
            if (tmrLlenado.Enabled == true)
            {
                pbxLlenado.BackColor = Color.Lime;
            }
            else
            {
                pbxLlenado.BackColor = Color.FromArgb(0, 32, 0);
            }

            if (tmrVaciado.Enabled == true)
            {
                pbxVaciado.BackColor = Color.Lime;
            }
            else
            {
                pbxVaciado.BackColor = Color.FromArgb(0, 32, 0);
            }

            if (tmrLlenado.Enabled == false && tmrVaciado.Enabled == false)
            {
                pbxParo.BackColor = Color.Red;
            }
            else
            {
                pbxParo.BackColor = Color.FromArgb(32, 0, 0);
            }
        }

        private void btnParo_Click(object sender, EventArgs e)
        {
            tmrVaciado.Enabled = false;
            tmrLlenado.Enabled = false;
        }
    }
}
